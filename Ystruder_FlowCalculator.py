import time
import numpy as np
import math

while(True):
    d = float(input("Syringe diameter [mm]: "))
    w = float(input("Extusion width [mm]: "))
    h = float(input("Layer height [mm]: "))
    v = float(input("Printing speed [mm/s]: "))

    A_path = (w-h)*h+np.pi*(h/2)**2
    Q = A_path * v
    v_piston = (4*Q)/(np.pi*d**2)

    print ("Piston speed [mm/s]: {:0.4f}".format(v_piston))
    variable = input('Enter q to quit, any other button to recalculate: ')
    if (variable == 'q'):
        break
