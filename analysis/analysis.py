import matplotlib.pyplot as plt
from matplotlib import rc
import numpy as np
import pandas as pd
import sys
import os
from glob import glob, iglob
from tabulate import tabulate
from scipy.optimize import curve_fit
from scipy.stats import linregress, sem
from unit_conversions import *
from slope import *
from errors import *
#Remove SettingWithCopy warning in Pandas
pd.options.mode.chained_assignment = None

#For debugging
try:
    from icecream import ic
except ImportError:  # Graceful fallback if IceCream isn't installed.
    ic = lambda *a: None if not a else (a[0] if len(a) == 1 else a)  # noqa


def all_params(path):
    df = pd.read_csv(path)
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,
                                                 2,
                                                 figsize=(20 / 2.54, 6 / 2.54))

    position = ax1.plot(df.time, df.distance, 'rs-', label='3')
    ax1.set(title='distance', ylabel='distance', xlabel='time (s)')

    mass = ax2.plot(df.time, df.mass, 'rs-', label='3')
    ax2.set(title='mass', ylabel='mass', xlabel='time (s)')

    force = ax3.plot(df.time, force_to_bar(df.force), 'kv-', label='2')
    ax3.set(title='force', ylabel='force', xlabel='time (s)')

    position = ax4.plot(df.time, adc_to_bar(df.adc), 'rs-', label='3')
    ax4.set(title='adc', ylabel='pressure', xlabel='time (s)')
    ratio = list((df.force / df.adc))
    fig.suptitle(path)
    plt.show()


def plot_set(path, func):
    sampleSet = iglob(path)  # Generate a list of all folders containing data
    for samples in sampleSet:
        func(samples)


def plot_pressures(path):
    df = pd.read_csv(path)
    smc = plt.plot(df.time, adc_to_bar(df.adc), label="Pressure sensor")
    ystr = plt.plot(df.time, df.force, label="Load cell")
    plt.title(path)
    plt.legend()
    plt.show()


def distances(path):
    df = pd.read_csv(path)
    df.distance = (-df.distance) + df.distance[0]
    distance = plt.plot(df.time, mm2ml(df.distance), label="Sylvac")
    df.mass = df.mass - df.mass[0]
    if ("1000cSt" in path):
        kinvisc = 1000
    elif ("10000cSt" in path):
        kinvisc = 10000
    mass = plt.plot(df.time, mass_to_volume(df.mass,kinvisc), label="Mettler")
    df.steps = df.steps - df.steps[0]
    steps = plt.plot(df.time, steps2ml(df.steps), label="Steps")
    plt.xlabel("Time (s)")
    plt.ylabel("Dose (ml)")
    plt.legend()
    plt.show()


def dist_pressure(path):
    df = pd.read_csv(path)
    df.distance = (-df.distance) + df.distance[0]
    fig, ax1 = plt.subplots()
    distance = ax1.plot(df.time,
                        mm2ml(df.distance),
                        label="Distance, dial indicator")
    df.steps = df.steps - df.steps[0]
    steps = ax1.plot(df.time,
                     steps2ml(df.steps),
                     label="Distance, reported by controller")
    ax2 = ax1.twinx()

    force = df.force - df.force[0]
    ax2.plot(df.time, df.force, 'b-', label="Load cell")
    ax2.plot(df.time, adc_to_force(df.adc), 'r-', label="Pressure sensor")

    ax1.set_xlabel('Time (s)')
    ax1.set_ylabel('Distance (mm)')
    ax2.set_ylabel('Force (N)')
    ax1.legend(loc='upper left')
    ax2.legend(loc='lower right')
    # plt.title(path)
    plt.show()


def tare_df(df):
    df.distance = (-df.distance) + df.distance[0]
    df.mass = df.mass - df.mass[0]
    df.force = df.force - df.force[0]
    return df


def force_and_movements():
    df1 = pd.read_csv(
        "./data/repeatibility/10000cSt/G15/1ml_min/1_mlmin_2_ml_1.csv")
    df2 = pd.read_csv(
        "./data/repeatibility/1000cSt/G21/1ml_min/1_mlmin_2_ml_2.csv")
    df3 = pd.read_csv(
        "./data/repeatibility/10000cSt/G21/02ml_min/0.2_mlmin_0.4_ml_9.csv")
    df4 = pd.read_csv(
        "./data/repeatibility/1000cSt/G21/02ml_min/0.2_mlmin_0.4_ml_0.csv")
    tare_df(df1)
    tare_df(df2)
    tare_df(df3)
    tare_df(df4)
    force_lim = [-4, 84]
    tick_labels = np.arange(0, 82, 20)
    time_lim = [-10, 310]
    fig, [axarr1, axarr2] = plt.subplots(2, 2, figsize=(7.5, 3.5))

    ######## TOP ROW #############

    axarr1[0].plot(df1.time, mm2ml(df1.distance), label="Dial indicator")
    axarr1[0].plot(df1.time, mass_to_volume(df1.mass, 10000), label="Scale")

    top_ylim = [-0.1, 2.1]
    top_yticks = np.arange(0.0, 2.1, 0.5)

    force_axis_1 = axarr1[0].twinx()
    force_axis_1.plot(df1.time, df1.force, 'r-', label="Force (N)")
    force_axis_1.set_ylim(force_lim)
    force_axis_1.tick_params(labelright=False)

    axarr1[0].set_xlabel('Time (s)')
    axarr1[0].set_ylabel('Dose (ml)')
    axarr1[0].set_xlim(time_lim)
    axarr1[0].set_ylim(top_ylim)
    axarr1[0].set_title("10000 cSt, 1 ml/min, 1.8 mm")
    axarr1[0].set_yticks(top_yticks)
    axarr1[0].grid(True)

    force_axis_2 = axarr1[1].twinx()
    force_axis_2.plot(df2.time, df2.force, 'r-', label="Force (N)")
    force_axis_2.set_ylabel('Force (N)')
    force_axis_2.set_ylim(force_lim)
    force_axis_2.set_yticks(tick_labels)

    axarr1[1].plot(df2.time, mm2ml(df2.distance), label="Dial indicator")
    axarr1[1].plot(df2.time, mass_to_volume(df2.mass, 1000), label="Scale")
    axarr1[1].set_title("1000 cSt, 1 ml/min, 0.5 mm")
    axarr1[1].set_xlim(time_lim, )
    axarr1[1].set_ylim(top_ylim)
    axarr1[1].set_xlabel('Time (s)')
    axarr1[1].tick_params(labelleft=False)
    axarr1[1].set_yticks(top_yticks)
    axarr1[1].grid(True)

    ######## BOTTOM ROW #############
    axarr2[0].plot(df3.time, mm2ml(df3.distance), label="Dial indicator")
    axarr2[0].plot(df3.time, mass_to_volume(df3.mass,10000), label="Scale")
    bottom_yticks = np.arange(0.0, 0.45, 0.1)
    bottom_ylim = [-0.02, 0.42]

    force_axis_3 = axarr2[0].twinx()
    force_axis_3.plot(df3.time, df3.force, 'r-')
    force_axis_3.set_ylim(force_lim)
    force_axis_3.tick_params(labelright=False)

    axarr2[0].set_xlabel('Time (s)')
    axarr2[0].set_ylabel('Dose (ml)')
    axarr2[0].set_xlim(time_lim)
    axarr2[0].set_ylim(bottom_ylim)
    axarr2[0].set_yticks(bottom_yticks)

    axarr2[0].set_title("10000 cSt, 0.2 ml/min, 0.5 mm")
    axarr2[0].grid(True)

    force_axis_4 = axarr2[1].twinx()
    force_axis_4.plot(df4.time, df4.force, 'r-', label="Force (N)")
    force_axis_4.set_ylabel('Force (N)')
    force_axis_4.set_ylim(force_lim)
    force_axis_4.set_yticks(tick_labels)
    axarr2[1].plot(df4.time, mm2ml(df4.distance), label="Dial indicator")
    axarr2[1].plot(df4.time, mass_to_volume(df4.mass,1000), label="Scale")
    axarr2[1].set_title("1000 cSt, 0.2 ml/min, 0.5 mm")
    axarr2[1].set_xlim(time_lim)
    axarr2[1].set_ylim(bottom_ylim)
    axarr2[1].set_xlabel('Time (s)')
    axarr2[1].set_yticks(bottom_yticks)
    axarr2[1].tick_params(labelleft=False)
    axarr2[1].grid(True)

    plt.tight_layout()
    plt.show()


def single_set(dataSet):
    parallels = iglob(dataSet)  # Generate a list of parallel measurements
    sample_count = 0
    for measurement in parallels:
        sample_count += 1
        df = pd.read_csv(measurement)
        df = df.drop(['millis', 'adc', 'state', 'time', 'samples'],
                     axis=1)  # These columns are not needed for this analysis
        df.distance = mm2ml(
            (-df.distance) + df.distance[0])  # Flip and zero sylvac
        if ("1000cSt" in measurement):
            kinvisc = 1000
        elif("10000cSt" in measurement):
            kinvisc = 10000
        df.mass = mass_to_volume((df.mass - df.mass[0]), kinvisc)  # Tare scale
        df.steps = steps2ml(df.steps - df.steps[0])  # Zero the steps
        df_max = df.agg(['max'])
        ic(df_max)
        row_name = measurement.split("/")[3] + "_" + measurement.split(
            "/")[4] + "_" + measurement.split("/")[5] + "_" + measurement[-6:]
        if ("02ml_min" in measurement):
            targetDose = 0.4
            targetSpeed = 0.2
        elif ("1ml_min" in measurement):
            targetDose = 2.0
            targetSpeed = 1.0
        else:
            raise ValueError('No speed in dataframe name')
        df_max.index = [row_name]
        fw_dict = calculate_steady_flowrate(measurement, True)
        fw_df = pd.Series(fw_dict).to_frame().T
        fw_df.index = [row_name]
        combined = pd.concat([df_max, fw_df], axis=1, sort=False)
        combined['mass_e'] = 100 * abs(combined.mass - targetDose) / targetDose
        combined['distance_e'] = 100 * abs(combined.distance -
                                           targetDose) / targetDose
        combined['flowrate_e'] = 100 * abs(combined.flowrate -
                                           targetSpeed) / targetSpeed
        combined['speed_e'] = 100 * abs(combined.speed -
                                        targetSpeed) / targetSpeed
        try:
            df_all = df_all.append(combined)
        except NameError:
            df_all = combined
    ic(df_all)
    funcs = ['mean', 'std', 'sem']
    df_set = df_all.agg(funcs)
    out_colnames = ["sample_count"]
    out_values = [sample_count]
    for colname in list(df_set):
        for func in funcs:
            out_colnames.append(colname + "_" + func)
            out_values.append(df_set.loc[func, colname])
    out_name = [
        dataSet.split("/")[3] + "_" + dataSet.split("/")[4] + "_" +
        dataSet.split("/")[5]
    ]
    df_out = pd.DataFrame([out_values], columns=out_colnames, index=out_name)
    ic(df_out)
    #Return a single row with dataset name as index and the name of func with variable
    return (df_out)


def all_sets(path):
    dataFolders = iglob(path)  # Generate a list of all folders containing data
    for dataSet in dataFolders:
        try:
            df_all = df_all.append(single_set(dataSet + "/*"))
        except NameError:
            df_all = single_set(dataSet + "/*")
    df_all.to_csv("./data/accuracy.csv")
    return df_all


def error_plot():
    path = "./data/accuracy.csv"
    df_err = pd.read_csv(path, index_col=0)
    df_err = df_err.sort_values(by=['force_mean'])
    ic(df_err)
    df_err = df_err.drop(df_err.index[-1])  #Drop last row due to stalling
    df_err = df_err.drop(df_err.index[-1])  #Drop last row due to stalling

    df_02 = df_err[~df_err.index.str.contains('1ml_min')]
    df_1 = df_err[~df_err.index.str.contains('02ml_min')]
    ic(df_1)
    width = 0.20
    fig, ax = plt.subplots(1, 2, sharey=True)

    pos1 = list(range(len(df_1.force_mean)))
    ax[0].bar(pos1,
              df_1.mass_e_mean,
              width,
              alpha=0.5,
              color='#EE3224',
              label=df_1.index[0],
              yerr=df_1.mass_e_sem,
              error_kw=dict(linewidth=2, capsize=5))
    ax[0].bar([p + width for p in pos1],
              df_1.distance_e_mean,
              width,
              alpha=0.5,
              color='#F78F1E',
              label=df_1.index[1],
              yerr=df_1.distance_e_sem,
              error_kw=dict(linewidth=2, capsize=5))
    ax[0].bar([p + width * 2 for p in pos1],
              df_1.flowrate_e_mean,
              width,
              alpha=0.5,
              color='#FFC222',
              label=df_1.index[2],
              yerr=df_1.flowrate_e_sem,
              error_kw=dict(linewidth=2, capsize=5))
    ax[0].bar([p + width * 3 for p in pos1],
              df_1.speed_e_mean,
              width,
              alpha=0.5,
              color='#1FC126',
              label=df_1.index[2],
              yerr=df_1.speed_e_sem,
              error_kw=dict(linewidth=2, capsize=5))

    pos02 = list(range(len(df_02.force_mean)))
    ax[1].bar(pos02,
              df_02.mass_e_mean,
              width,
              alpha=0.5,
              color='#EE3224',
              label=df_02.index[0],
              yerr=df_02.mass_e_sem,
              error_kw=dict(linewidth=2, capsize=5))
    ax[1].bar([p + width for p in pos02],
              df_02.distance_e_mean,
              width,
              alpha=0.5,
              color='#F78F1E',
              label=df_02.index[1],
              yerr=df_02.distance_e_sem,
              error_kw=dict(linewidth=2, capsize=5))
    ax[1].bar([p + width * 2 for p in pos02],
              df_02.flowrate_e_mean,
              width,
              alpha=0.5,
              color='#FFC222',
              label=df_02.index[2],
              yerr=df_02.flowrate_e_sem,
              error_kw=dict(linewidth=2, capsize=5))
    ax[1].bar([p + width * 3 for p in pos02],
              df_02.speed_e_mean,
              width,
              alpha=0.5,
              color='#1FC126',
              label=df_02.index[2],
              yerr=df_02.speed_e_sem,
              error_kw=dict(linewidth=2, capsize=5))

    ax[0].set_ylabel('Error (\%)')
    ForceLabels_1 = []
    Labels_1 = []
    for index, row in df_1.iterrows():
        if ("G21" in index):
            D_str = "0.5"
        elif ("G15" in index):
            D_str = "1.8"
        else:
            raise ValueError('No needle gauge in dataframe name')
        if ("1000cSt" in index):
            nu_str = "1000"
        elif ("10000cSt" in index):
            nu_str = "10000"
        else:
            raise ValueError('No kinematic viscosity in dataframe name')
        F_str = str(round(row.force_mean, 1)) + "$\pm$" + str(
            round(row.force_sem, 1))
        Labels_1.append(D_str + "\n" + nu_str)
        ForceLabels_1.append(F_str)
    ax[0].set_xticks([p + 1.5 * width for p in pos1])
    for idx, p in enumerate(pos1):
        ax[0].text(p + 1.1 * width, -1.2, Labels_1[idx])
    # ax[0].set_xticks([p + 1.5 * width for p in pos1])
    ax[0].set_xticklabels(ForceLabels_1)
    ax[0].set_title('1 ml / min')
    ax[0].legend(
        ['Dose error', 'Distance error', 'Flow rate error', 'Speed error'],
        loc='upper left')
    ax[0].grid()

    ForceLabels_02 = []
    Labels_02 = []
    for index, row in df_02.iterrows():
        if ("G21" in index):
            D_str = "0.5"
        elif ("G15" in index):
            D_str = "1.8"
        else:
            raise ValueError('No needle gauge in dataframe name')
        if ("1000cSt" in index):
            nu_str = "1000"
        elif ("10000cSt" in index):
            nu_str = "10000"
        else:
            raise ValueError('No kinematic viscosity in dataframe name')
        F_str = str(round(row.force_mean, 1)) + "$\pm$" + str(
            round(row.force_sem, 1))
        Labels_02.append(D_str + "\n" + nu_str)
        ForceLabels_02.append(F_str)

    ax[1].set_xticklabels(ForceLabels_02)
    ax[1].set_xticks([p + 1.5 * width for p in pos02])
    for idx, p in enumerate(pos02):
        ax[1].text(p + 1.1 * width, -1.2, Labels_02[idx])
    ax[1].set_title('0.2 ml / min')
    ax[1].grid()

    ax[0].text(-0.7, -0.2, "Force (N)")
    ax[0].text(-0.7, -0.5, "Diameter (mm)")
    ax[0].text(-0.7, -1.0, "Kinematic viscosity (cSt)")
    plt.tight_layout()

    plt.show()


if __name__ == '__main__':
    if sys.version_info[0] < 3:
        raise Exception("Must be using Python 3")
    # Type the name of the function you want to run here 

    # To plot the measurements from a single data set
    df_set = single_set("./data/repeatibility/1000cSt/G21/1ml_min/*")
  
    # To plot the distance and pressure data from a single measurement
    # dist_pressure("./data/plugged/0.2_mlmin_1.4_ml_0.csv")

    # To plot the distances of a set 
    # plot_set("data/repeatibility/10000cSt/G21/1ml_min/*", distances)

    # To get a dataframe of all the repeatibility measurements and export it to accuracy.csv
    # df_all = all_sets("./data/repeatibility/***/**/*")

    # Plot the errors (from accuracy.csv) of the repeatibility in a bar chart
    # error_plot()
