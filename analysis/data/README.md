# Description of data

## Plugged
Measurements of pressure inside a plugged syringe.

## Repeatibility
Parallel measurements using different syringe orifice sizes, viscosities and speeds.
Pressure sensor disconnected, adc column is in the data but it basically noise

## accuracy.csv
This is an intermediary .csv that contains the dose, movement, flow rate and speed values computed from the measurements along with the associated errors.
