# from sklearn import linear_model
import pandas as pd
from math import factorial
import numpy as np
from unit_conversions import *
import matplotlib.pyplot as plt


def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    # https://scipy-cookbook.readthedocs.io/items/SavitzkyGolay.html
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except (ValueError, msg):
        raise ValueError("window_size and order have to be of type int")
    if (window_size % 2 != 1 or window_size < 1):
        raise TypeError("window_size size must be a positive odd number")
    if (window_size < order + 2):
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def findLinear(x,y, minLength):
    chi = 0
    chi_min = 10000
    startPoint = 0
    stopPoint = 0
    for i in range(len(x) - minLength):
        for j in range(i+minLength, len(x)):
            coefs = np.polyfit(x[i:j],y[i:j],deg=1)
            y_linear = x * coefs[0] + coefs[1]
            chi = 0
            for k in range(i,j):
                chi += ( y_linear[k] - y[k])**2
            if chi < chi_min:
                startPoint = i
                stopPoint = j
                chi_min = chi
                ic(chi)
    coefs = np.polyfit(x[startPoint:stopPoint],y[startPoint:stopPoint],deg=1)
    return coefs,startPoint,stopPoint


def calculate_coefs(df,smooth,deltaThreshold):
    if (smooth):    # We apply a Savitzky-Golay filter to smooth it
        df.y = savitzky_golay(df.y.values, 81, 3)
    maxDelta = np.max(df.y.diff())
    df = df.drop(df[df.y.diff()<deltaThreshold*maxDelta].index)   
    df = df.drop(df.index[:50])           # drop 50 samples from beginning, so that initial acceleration does not affect polyfit
    start = df.index[0]
    stop = df.index[-1]
    coefs= np.polyfit(df.x,df.y,deg=1)
    out_dict = {'coefs':coefs,'start':start,'stop':stop}
    return out_dict

def calculate_steady_flowrate (path,showPlot):
    # https://stackoverflow.com/questions/13691775/python-pinpointing-the-linear-part-of-a-slope
    df = pd.read_csv(path)
    df = df.drop(['millis','adc','state','force', 'samples'], axis=1)            # These columns are not needed for this analysis
    df.distance = -df.distance + df.distance[0]
    df.mass = df.mass - df.mass[0]
    df.steps = df.steps - df.steps[0]

    df_m = df[["time","mass"] ]
    df_m = df_m.rename(columns={'mass': 'y', 'time':'x'})
    df_d = df[["time","distance"] ]
    df_d = df_d.rename(columns={'distance': 'y', 'time':'x'})
    df_s = df[["time","steps"] ]
    df_s = df_s.rename(columns={'steps': 'y', 'time':'x'})

    mass_steady = calculate_coefs(df_m, True, 0.6)
    distance_steady = calculate_coefs(df_d, False, 0.2)
    steps_steady = calculate_coefs(df_s, False, 0.2)
    if ("1000cSt" in path):
        kinvisc = 1000
    elif ("10000cSt" in path):
        kinvisc = 10000
    mass_flowrate = mass_to_volume(mass_steady['coefs'][0] * 60,kinvisc)
    distance_flowrate = mm2ml(distance_steady['coefs'][0]) * 60
    steps_flowrate = steps2ml(steps_steady['coefs'][0]) * 60
    flowrate_dict = {'flowrate':mass_flowrate, 'speed':distance_flowrate,'steps_speed':steps_flowrate}
    
    if (showPlot):
        start_m = df_m.x[mass_steady['start']]
        stop_m = df_m.x[mass_steady['stop']]
        start_d = df_d.x[mass_steady['start']]
        stop_d = df_d.x[mass_steady['stop']]
        start_s = df_s.x[steps_steady['start']]
        stop_s = df_s.x[steps_steady['stop']]

        fitted_mass = df_m.x*mass_steady['coefs'][0] + mass_steady['coefs'][1]      
        fitted_distance = df_d.x*distance_steady['coefs'][0] + distance_steady['coefs'][1]      
        fitted_steps = df_s.x*steps_steady['coefs'][0] + steps_steady['coefs'][1]      

        fig,(ax1,ax2,ax3) = plt.subplots(1,3,figsize=(12,8))

        ax1.plot(df_m.x,df_m.y, 'bo-',label='extracted section')
        fitMassLabel = "polyfit, "+ str(round(mass_flowrate,3))+" ml min"
        ax1.plot(df_m.x,fitted_mass,label=fitMassLabel,color='purple')
        ax1.axvspan(start_m,stop_m, color="y", alpha=0.2)
        ax1.set(title='Mass', ylabel='ml', xlabel='time')
        ax1.legend(loc="upper left")

        ax2.plot(df_d.x,df_d.y, 'ro-',label='extracted section1')
        fitDistLabel = "polyfit, "+ str(round(distance_flowrate,3))+" ml min"
        ax2.plot(df_d.x,fitted_distance,label = fitDistLabel)
        ax2.axvspan(start_d,stop_d, color="y", alpha=0.2)
        ax2.set(title='Distance', ylabel='mm', xlabel='time')
        ax2.legend(loc="upper left")

        ax3.plot(df_s.x,df_s.y, 'ro-',label='extracted section1')
        fitStepLabel = "polyfit, "+ str(round(steps_flowrate,3))+" ml min"
        ax3.plot(df_s.x,fitted_steps,label = fitStepLabel)
        ax3.axvspan(start_s,stop_s, color="y", alpha=0.2)
        ax3.set(title='Steps', ylabel='steps', xlabel='time')
        ax3.legend(loc="upper left")

        fig.suptitle(path) 
        plt.show()
        return flowrate_dict
    else:
        return flowrate_dict


