from unit_conversions import *
import pandas as pd
import numpy as np
from glob import glob,iglob
import re

def parseTargetValues(row):
    speed_str = row.name.split("_")[2]
    speed_str = re.findall("\d+", speed_str)[0]
    if speed_str[0] == "0":
        target_flowrate = float(speed_str) / 10
    else:
        target_flowrate = float(speed_str)
    return target_flowrate

def computeError(row,type_str,unit_str):
    value_mean = unit_str + "_mean" 
    value_target = "target_"+type_str
    return 100*abs(row[value_mean]-row[value_target])

def computeCV(row,type_str):                # aka standard deviation of the mean aka repeatibility
    value_mean = type_str + "_mean" 
    value_std = type_str + "_std" 
    return 100*(row[value_std]/row[value_mean])

def errors(path):
    try:
        df = pd.read_csv(path,index_col=0)
    except FileNotFoundError:
        print("Could not find the csv")
        return
    # ic(df.columns)
    flowrate_cols =['mass_flowrate_mean','mass_flowrate_std', 'distance_flowrate_mean','distance_flowrate_std','steps_flowrate_mean', 'steps_flowrate_std'] 
    df_flowrate = df[flowrate_cols]
    dose_cols =  ['distance_mean','distance_std', 'mass_mean','mass_std',  'steps_mean','steps_std']
    df_dose = df[dose_cols] 
    df_flowrate['target_flowrate'] = df_flowrate.apply(parseTargetValues,axis = 1)
    df_dose['target_dose'] = 2 * df_flowrate.target_flowrate        # All samples were run with a two minute dosing cycle with means our intended dose is 2 times the speed
    
    df_dose['mass_e'] = df_dose.apply(computeError, args=["dose","mass"], axis=1)
    df_dose['mass_cv'] = df_dose.apply(computeCV,args=["mass"], axis=1)
    df_dose['distance_e'] = df_dose.apply(computeError, args=["dose","distance"], axis=1)
    df_dose['distance_cv'] = df_dose.apply(computeCV,args=["distance"], axis=1)
    # df_dose['steps_e'] = df_dose.apply(computeError, args=["dose","steps"], axis=1)
    # df_dose['steps_cv'] = df_dose.apply(computeCV,args=["steps"], axis=1)

    df_flowrate['flowrate_mass_e'] = df_flowrate.apply(computeError, args=["flowrate","mass_flowrate"], axis=1)
    df_flowrate['flowrate_mass_cv'] = df_flowrate.apply(computeCV,args=["mass_flowrate"], axis=1)
    df_flowrate['flowrate_distance_e'] = df_flowrate.apply(computeError, args=["flowrate","distance_flowrate"], axis=1)
    df_flowrate['flowrate_distance_cv'] = df_flowrate.apply(computeCV,args=["distance_flowrate"], axis=1)
    # df_flowrate['flowrate_steps_e'] = df_flowrate.apply(computeError, args=["flowrate","steps_flowrate"], axis=1)
    # df_flowrate['flowrate_steps_cv'] = df_flowrate.apply(computeCV,args=["steps_flowrate"], axis=1)

    dose_errors = df_dose.drop(dose_cols,axis=1)
    dose_errors = dose_errors.drop(['target_dose'],axis=1)
    flowrate_errors = df_flowrate.drop(['target_flowrate'],axis=1)
    flowrate_errors = flowrate_errors.drop(flowrate_cols,axis=1)
    errors = pd.concat([df.force_mean,dose_errors, flowrate_errors], axis=1, sort=False)
    # with open("errors.tex","w") as f: 
        # f.write(errors.round(2).to_latex())
    return errors
    # coefficent_variation = 100 * std/mean   
    # df = df(['millis','adc','state','time', 'samples'], axis=1)            # These columns are not needed for this analysis
    # df = df[["distance_mean","distance_std"] ]



