import numpy as np

def mass_to_volume(mass, kinvisc):
    if (kinvisc == 1000):
        siliconeDensity = 0.955         # Measurement: 0.9547 ± 0.02 g/mL
    elif (kinvisc == 10000):
        siliconeDensity = 0.958         # Measurement: 0.9584 ± 0.02 g/mL
    return (mass / siliconeDensity)

def adc_to_force(adc):
    #Pressure sensor output 1 to 5 V corresponding to 0 to 10 bar relative, adc is 16-bit (0 to 65535) with max input voltage of 6.144
    # Max pressure is 10 bar which is about 200 N
    voltage = adc*0.00018751       #6.144/32767
    pressure = (voltage-1) * 2.5    # 2.5 = 10 / 4 (max value in bar / (max value of volts - offset))
    force = (pressure*1e5*np.pi*(8e-3) ** 2)
    return(force)    

def force_to_bar(force):
    A = np.pi*(8e-3) ** 2           # Syringe inner radius 8 mm
    return (force/A)/1e5

def mm2ml (mm):
    A = np.pi*(8e-3) ** 2       # Cross-sectional area in m^3
    V = mm*1e-3 * A             # Dosed volume in mm^3
    return (V/1e-6)             # Return value in ml

def steps2ml (steps):
    steps_per_revolution = 200
    microsteps = 16
    screw_pitch = 2
    mm = screw_pitch * steps / (steps_per_revolution * microsteps)
    return(mm2ml(mm))


