from ADS1115  import *     # 16-bit ADC for pressure sensor
from Sylvac   import *     # Linear gauge, serial port
from Mettler  import *     # Scale, serial port
from YStruder import *     # YStuder commands, serial port
import csv
import os
from datetime import datetime
from time import sleep 
import sys

def main():
    ads1115 = ADS1115()
    sylvac = Sylvac('/dev/ttyUSB0') 
    mettler = Mettler('/dev/ttyUSB1') 
    ystruder = YStruder('/dev/ttyUSB2') 
    ads1115.start()
    sylvac.start()
    mettler.start()
    flowrate = 1.5                      # Flowrate in ml/min
    dose = 2.0                 
    ystruder.start(dose,flowrate)
    previousReadings = [0,0,0,0,0]
    data = []  
    start = datetime.now()
    postDosePeriod = 0                                 # Time (in seconds) to keep measuring after extrusion complete TODO: Use force to determine when the dose has "bled" out
    extrusionComplete = 0
    while True:
        try:
            currentSample = datetime.now()
            seconds = (currentSample-start).seconds
            hundreds  = (currentSample-start).microseconds/1000000
            timestamp = round(float(seconds+hundreds),2)
            msg = ystruder.read()
            readings = tuple(filter(None, msg.split('\t')))
            if (not extrusionComplete):
                if (readings[1] != previousReadings[1]):        # Check if it is a new sample 
                    data.append([timestamp, ads1115.read(), sylvac.read(), mettler.read(), int(readings[0]), int(readings[1]),int(readings[2]), float(readings[3]), str(readings[4])])
                if (readings[4] == 'i' and previousReadings[4] == 'p'):                      # Transition from p to i marks end of extrusion
                    print("Extrusion complete, starting post dose measurement period")
                    extrusionComplete = currentSample
                previousReadings = readings
            else:
                if(currentSample-extrusionComplete).seconds <= postDosePeriod:
                    if (readings[1] != previousReadings[1]):
                        data.append([timestamp, ads1115.read(), sylvac.read(), mettler.read(), int(readings[0]), int(readings[1]),int(readings[2]), float(readings[3]), str(readings[4])])
                        previousReadings = readings
                else:
                    print("Ending measurement")
                    break
        except KeyboardInterrupt:
            print("Keyboard interrupt")
            break
    ads1115.close()
    mettler.close()
    sylvac.close()
    ystruder.close()
    i = 0
    while os.path.exists("../data/%s_mlmin_%s_ml_%s.csv" % (flowrate,dose,i)):
        i += 1
    filestr = "../data/%s_mlmin_%s_ml_%s.csv" % (flowrate,dose,i)
    file = open(filestr, "w")
    file.write("time,adc,distance,mass,millis,samples,steps,force,state\n")
    cw = csv.writer(file)
    for sample in data:
        cw.writerow(sample)
    file.close()
    print("done")

if __name__ == '__main__':
    if sys.version_info[0] < 3:
        raise Exception("Must be using Python 3")
    main()
