from threading import Thread
from time import sleep
import serial
import time
import collections
import struct
import copy

class Mettler:
    def __init__(self, serialPort='/dev/ttyUSB2', serialBaud=9600, sampleInterval=0.2):
        self.port = serialPort
        self.baud = serialBaud
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        self.rawData = 0 
        self.interval = sampleInterval
        print('Trying to connect to: ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        try:
            self.serialConnection = serial.Serial(
                                                port=serialPort,
                                                baudrate=serialBaud,
                                                parity=serial.PARITY_NONE,
                                                stopbits=serial.STOPBITS_ONE,
                                                bytesize=serial.EIGHTBITS,
                                                timeout = 0.2           
                                                )
            print('Mettler connected to ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        except:
            print("Failed to connect to Mettler with " + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')

    def start(self):
        if self.thread == None:
            self.thread = Thread(target=self.backgroundThread)
            self.thread.start()
            # Block untit values start coming
            while self.isReceiving != True:
                time.sleep(0.5)

    def read(self):
        privateData = copy.deepcopy((self.rawData.decode("utf-8")))
        if (len(privateData)<3):
            return float('nan')
        else:
            privateData = privateData.split()[2]                    #Extract just the scale value
            return (float(privateData))

    def backgroundThread(self):    
        time.sleep(self.interval)                                       # How often do we retrieve scale value 
        self.serialConnection.reset_input_buffer()
        while (self.isRun):
            self.serialConnection.write(b'SI\r\n')                      # Send 'S' for stable value, 'SI' for immediate
            self.rawData = self.serialConnection.readline()             # read everything in the input buffer
            self.rawData = self.rawData.rstrip()
            self.isReceiving = True
 
    def close(self):
        self.isRun = False
        self.thread.join()
        self.serialConnection.close()
        print('Mettler disconnected...')
 
if __name__ == '__main__':
    mettler = Mettler()
    mettler.start()
    i = 0
    try:
        while True:
            sleep(1)
            print("sample: %d \t reading %.2f" % (i,mettler.read() ))
            i += 1
    except KeyboardInterrupt:
        print("closing")
        mettler.close()
        pass






