from threading import Thread
from time import sleep
import serial
import time
import collections
import struct
import copy


class Sylvac:
    def __init__(self, serialPort='/dev/ttyUSB0', serialBaud=4800, sampleInterval=0.2):
        self.port = serialPort
        self.baud = serialBaud
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        self.rawData = 0 
        self.interval = sampleInterval
        print('Trying to connect to: ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        try:
            self.serialConnection = serial.Serial(
                                                port=serialPort,
                                                baudrate=serialBaud,
                                                parity=serial.PARITY_EVEN,
                                                stopbits=serial.STOPBITS_TWO,
                                                bytesize=serial.SEVENBITS,
                                                timeout = 0.2           
                                                )
            print('Sylvac connected to ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        except:
            print("Failed to connect to Sylvac with " + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')

    def start(self):
        if self.thread == None:
            self.thread = Thread(target=self.backgroundThread)
            self.thread.start()
            # Block till we start receiving values
            while self.isReceiving != True:
                time.sleep(0.1)

    def read(self):
        privateData = copy.deepcopy(self.rawData)    
        return (float(privateData))

    def backgroundThread(self):    # retrieve data
        time.sleep(self.interval)  # give some buffer time for retrieving data
        self.serialConnection.reset_input_buffer()
        while (self.isRun):
            self.serialConnection.write(b'A')                           #Send any ASCII character to get current measurement
            self.rawData = self.serialConnection.readline()             # read everything in the input buffer
            self.rawData = self.rawData.rstrip()
            self.isReceiving = True
 
    def close(self):
        self.isRun = False
        self.thread.join()
        self.serialConnection.close()
        print('Sylvac disconnected...')
 
if __name__ == '__main__':
    sylvac = Sylvac()
    sylvac.start()
    i = 0
    try:
        while True:
            sleep(0.5)
            print("sample: %d \t reading %.3f" % (i,sylvac.read() ))
            i += 1
    except KeyboardInterrupt:
        print("closing")
        sylvac.close()
        pass






