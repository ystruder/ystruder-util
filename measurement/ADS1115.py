from threading import Thread
from time import sleep
import smbus
import time
import collections
import struct
import copy

# ADS1115 address, 0x48(72)
# Select configuration register, 0x01(01)
# 0x8283(33411)	AINP = AIN0 and AINN = AIN1, +/- 4.096V
# Continuous conversion mode, 128SPS

class ADS1115:
    def __init__(self,default_address=0x48,sampleInterval=0.2):
        self.default_address = 0x48
        self.config_reg      = 0x01
        self.low_threshold   = 0x02
        self.high_threshold  = 0x03
        self.os_single       = 0x8000
        self.mux_offset      = 12
        self.channel = 1;
        self.gain = {
                2/3: 0x0000,
                1:   0x0200,
                2:   0x0400,
                4:   0x0600,
                8:   0x0800,
                16:  0x0A00
                }
        self.samplerate = {
                8:    0x0000,
                16:   0x0020,
                32:   0x0040,
                64:   0x0060,
                128:  0x0080,
                250:  0x00A0,
                475:  0x00C0,
                860:  0x00E0
        }
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        self.interval = sampleInterval
        self.config = self.os_single
        self.mux = self.channel + 0x04
        self.config |= (self.mux & 0x07) << self.mux_offset
        self.config |= self.gain[2/3]
        self.config |= self.samplerate[128]
        self.configbyte  = [(self.config >> 8) & 0xFF, self.config & 0xFF]
        self.bus = smbus.SMBus(1)
        self.bus.write_i2c_block_data(self.default_address, self.config_reg, self.configbyte)
        print("Trying to connect to: ADS1115")
        try:
            print("ADS1115 connected")
        except:
            print("Failed to connect to ADS1115")

    def start(self):
        if self.thread == None:
            self.thread = Thread(target=self.backgroundThread)
            self.thread.start()
            # Block untit values start coming
            while self.isReceiving != True:
                time.sleep(0.5)

    def read(self):
        privateData = copy.deepcopy(self.rawData)
        privateData = privateData[0] * 256 + privateData[1]              #raw_adc 
        if privateData > 32767:
            privateData -= 65535
        return (privateData)

    def backgroundThread(self):    
        time.sleep(self.interval)                                       
        while (self.isRun):
            self.rawData = self.bus.read_i2c_block_data(self.default_address, 0x00, 2)
            self.isReceiving = True
 
    def close(self):
        self.isRun = False
        self.thread.join()
        print('ADS115 closed...')
 
if __name__ == '__main__':
    ads1115 = ADS1115()
    ads1115.start()
    i = 0
    try:
        while True:
            sleep(1)
            voltage = float(ads1115.read()*0.00018751)       #6.144/32767
            print("sample: %d \t reading %.2f" % (i,voltage))
            i += 1
    except KeyboardInterrupt:
        print("closing")
        ads1115.close()
        pass
