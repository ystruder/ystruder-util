from threading import Thread
from time import sleep
import serial
import time
import collections
import struct
import copy

class YStruder:
    def __init__(self, serialPort='/dev/ttyUSB1', serialBaud=115200, sampleInterval=0.1):
        self.port = serialPort
        self.baud = serialBaud
        self.isRun = True
        self.isReceiving = False
        self.thread = None
        self.rawData = 0 
        self.interval = sampleInterval
        print('Trying to connect to: ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        try:
            self.serialConnection = serial.Serial(
                                                port=serialPort,
                                                baudrate=serialBaud,
                                                parity=serial.PARITY_NONE,
                                                stopbits=serial.STOPBITS_ONE,
                                                bytesize=serial.EIGHTBITS,
                                                timeout = 0.2           
                                                )
            print('YStruder connected to ' + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')
        except:
            print("Failed to connect to YStruder with " + str(serialPort) + ' at ' + str(serialBaud) + ' BAUD.')

    def start(self,dose,flowrate):
        self.serialConnection.write(b"Dose %f\r\n" % dose)
        self.msg = self.serialConnection.readline()             # read everything in the input buffer
        print(self.msg)
        self.serialConnection.write(b"Rate %f\r\n" % flowrate)
        self.msg = self.serialConnection.readline()             # read everything in the input buffer
        print(self.msg)
        self.serialConnection.write(b'Run\r\n')
        self.msg = self.serialConnection.readline()             # read everything in the input buffer
        print(self.msg)
        if self.thread == None:
            self.thread = Thread(target=self.backgroundThread)
            self.thread.start()
            while self.isReceiving != True:                     # Block untit values start coming
                time.sleep(0.5)

    def read(self):
        privateData = copy.deepcopy((self.rawData.decode("utf-8")))
        return privateData

    def backgroundThread(self):    
        time.sleep(self.interval)                                         
        self.serialConnection.reset_input_buffer()
        while (self.isRun):
            self.serialConnection.write(b'Meas\r\n')                           
            self.rawData = self.serialConnection.readline()                
            self.rawData = self.rawData.rstrip()
            self.isReceiving = True
 
    def close(self):
        self.isRun = False
        self.thread.join()
        self.serialConnection.close()
        print('YStruder disconnected...')
 
if __name__ == '__main__':
    ystruder = YStruder()
    dose = 0.5                          # Dose in ml
    flowrate = 10                       # Flowrate in ml/min
    ystruder.start(dose,flowrate)
    varNames = ['millis','samples','position','force', 'state']
    previousReading = 0
    try:
        while True:
            msg = ystruder.read()
            if (msg):
                readings = tuple(filter(None, msg.split('\t')))
                if (readings != previousReading):
                    print(readings)
                    previousReading = readings
                if (readings[4] == 'i'):
                    print("extrusion done")
                    break
            sleep(0.1)
    except KeyboardInterrupt:
        print("Keyboard interrupt")

    ystruder.close()
